package ru.t1.dzelenin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.request.ServerAboutRequest;
import ru.t1.dzelenin.tm.dto.request.ServerVersionRequest;
import ru.t1.dzelenin.tm.dto.response.ServerAboutResponse;
import ru.t1.dzelenin.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}

