package ru.t1.dzelenin.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class ServerVersionResponse extends AbstractResponse {

    @NotNull
    private String version;

}

